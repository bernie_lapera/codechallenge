<?php
namespace app\controller;

class RouteController {

    /**
     * available routes,
     * @todo stupid custom mvc!!!, only supporting int URI vars...
     * @var array
     */
    private $routes = [
        'GET' => [
            '^/$' => ['AppController', 'index'],
//            '/foo/:i/:i' => ['AppController', 'foo'],
            '/review/paginate/:i' => ['AppController', 'paginate']
        ],
        'POST' => [
        ]
    ];

    /**
     * check and run route
     */
    public function __construct(){

        //check request method
        $method = $_SERVER['REQUEST_METHOD'];
        if(!isset($this->routes[$method])){
            exit('Route method not found');
        }

        //if ($method == 'POST') check csrf token...

        $uri = $_SERVER['REQUEST_URI'];
        foreach($this->routes[$method] as $path => $function){

            //check controller & controller method
            $controller = "\\app\\controller\\{$function[0]}";
            if(!method_exists($controller, $function[1])){
                exit('Controller method not found');
            }

            //match route & call function
            if(preg_match('/'. str_replace(['/', ':i'], ['\/', '([0-9]+)'], $path) .'/', $uri)){
                //URI parts
                $uriParts = explode('/', ltrim($uri, '/'));

                //route parts
                $routeParts = explode('/', ltrim($path, '/'));

                //extract route variables from route parts
                $param = [];
                foreach($routeParts as $index => $part){
                    if($part[0] == ':'){
                        $param[] = $uriParts[$index];
                    }
                }

                call_user_func_array([(new $controller()), $function[1]], $param);
                return;
            }
        }

        /**
         * generic fail end
         */
        exit('Route not found');
    }
}