<?php
namespace app\controller;

use app\model\RLApi;

class AppController {

    /**
     * create view
     * @todo in the "production model" this method should be a trait or an extended base controller class method...
     * @param $view
     * @param array $data
     * @return bool
     */
    private function view($view, array $data=[]){
        $file = '../app/view/'. $view .'.php';
        if(!file_exists($file)){
            header( 'Location: /' );
        }
        extract($data);
        include $file;
        return true;
    }

    public $reviewIcons = [
        0 => '/assets/img/social/company-icon.png',
        1 => '/assets/img/social/yelp.png',
        2 => '/assets/img/social/google-32x32.png',
    ];

    /**
     * render app index page
     * @return bool
     */
    public function index(){
        $rlApi = new RLApi();
        return $this->view('index', [
            'data' => $rlApi->data,
            'paginateLinks' => $this->paginateLinks($rlApi->data),
            'reviewList' => $this->reviewList($rlApi->data)
        ]);
    }

    public function paginate($page){
        $page = $page * 1;
        if(!is_int($page)){
            exit('Invalid Input');
        }

        $param = [];
        $param['offset'] = ($page == 1) ? 0 : ($page-1) * 20;


        $rlApi = new RLApi($param);
        if(isset($rlApi->data->errorMessage)){
            http_response_code(400);
            exit('No records found.');
        }

        $showing = 'showing review 1 to 20 of '. $rlApi->data->business_info->total_rating->total_no_of_reviews;
        if($page > 1){
            $showing = 'showing review '. ($param['offset'] + 1) .' to '. ($param['offset'] + 20) .' of '. $rlApi->data->business_info->total_rating->total_no_of_reviews;
        }

        header('Content-Type: application/json');
        echo json_encode([
            'paginateLinks' => $this->paginateLinks($rlApi->data, $page),
            'reviewList' => $this->reviewList($rlApi->data),
            'showing' => $showing
        ]);
        exit();
    }

    private function paginateLinks($data, $page=1){
        if(!is_int($page)){
            $page = 1;
        }
        $perPage = 20;
        $pages = ceil($data->business_info->total_rating->total_no_of_reviews / $perPage);

        if($page < 3){
            $start = 1;
        }
        elseif($page == $pages){
            $start = $pages - 5;
            $start = ($start > 0) ? $start : 1;
        }
        else{
            $start = $page - 3;
            $start = ($start > 0) ? $start : 1;
        }

        $paginate = [
            'page' => $page,
            'pages' => $pages,
            'start' => $start
        ];

        ob_start();
        include '../app/view/paginate.php';
        return ob_get_clean();
    }

    private function reviewList($data){
        $icons = $this->reviewIcons;
        ob_start();
        include '../app/view/reviewlist.php';
        return ob_get_clean();

    }

    public function foo($a, $b){
        echo __METHOD__;
        _printr(func_get_args());
        exit();
    }
}