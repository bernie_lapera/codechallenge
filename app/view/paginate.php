<li<?= ($paginate['page'] == 1 ? ' class="disabled"' : '') ?>>
    <a href="#" aria-label="Previous" data-page="<?= ($paginate['page'] - 1) ?>">
        <span aria-hidden="true">&laquo;</span>
    </a>
</li>
<?php
for($i=$paginate['start']; $i <= ($paginate['start'] + 5); $i++){
    if($i <= $paginate['pages']){
        echo '<li'. ($i == $paginate['page'] ? ' class="active"' : '') .'><a href="#" data-page="'. $i .'">'. $i .'</a></li>';
    }
}
?>
<li<?= ($paginate['page'] == $paginate['pages'] ? ' class="disabled"' : '') ?>>
    <a href="#" aria-label="Next" data-page="<?= ($paginate['page'] + 1) ?>">
        <span aria-hidden="true">&raquo;</span>
    </a>
</li>