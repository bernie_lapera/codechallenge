<?php
foreach($data->reviews as $review){
?>
    <div class="-review">
        <div class="-review-head clearfix">
            <img src="<?= (isset($icons[$review->review_from]) ? $icons[$review->review_from] : $icons[0])  ?>" class="-review-icon">
            <h4><?= $review->customer_name ?></h4>
            <div class="-stars">
                <?php
                for($i=1; $i<=5; $i++){
                    if($i <= $review->rating){
                        echo '<span class="glyphicon glyphicon-star"></span>';
                    }else{
                        echo '<span class="glyphicon glyphicon-star-empty"></span>';
                    }
                }
                ?>
            </div>
        </div>
        <div class="-review-body clearfix">
            <div class="-review-time">
                <?= date('l F jS Y', strtotime($review->date_of_submission)) ?>
            </div>
            <p><?= nl2br($review->description) ?></p>
        </div>
    </div>
<?php
}
?>