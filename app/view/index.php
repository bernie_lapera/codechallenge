<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/assets/img/favicon.ico">
    <title>Code Challenge</title>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href="/assets/css/app.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="-header clearfix">
        <div class="container">
            <h1>CodeChallenge<br><span>by Bernie Lapera</span></h1>
        </div>
    </div>
    <div class="-header-block container">
        <div class="row">
            <div class="col-lg-6">
                <h2><?= $data->business_info->business_name ?></h2>
                <p class="-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam commodo eget nisi nec rutrum. Vivamus tincidunt, orci accumsan luctus imperdiet, dui velit ornare odio, in accumsan ipsum eros eget augue. Donec sollicitudin euismod tincidunt. Donec imperdiet ut ipsum eget tempus. Vivamus nec metus sed magna pulvinar mollis. Cras dictum est sed quam accumsan efficitur. Suspendisse eu ante justo. Proin ullamcorper at libero a vulputate.</p>
                <div class="-info clearfix">
                    <span class="glyphicon glyphicon-pushpin"></span>
                    <span class="-info-val"><?= str_replace('<br>', ' ', $data->business_info->business_address) ?></span>
                </div>
                <div class="-info clearfix">
                    <span class="glyphicon glyphicon-phone"></span>
                    <span class="-info-val"><?= $data->business_info->business_phone ?></span>
                </div>
            </div>
            <div class="col-lg-6">
                <img src="/assets/img/laketahoe.jpg" class="img-responsive -business-img img-rounded">
            </div>
        </div>
    </div>
    <div class="-body-block container">
        <div class="row">
            <div class="col-lg-9">
                <div class="-review-header clearfix">
                    <h3>User Reviews</h3>
                </div>
                <nav class="clearfix">
                    <ul id="-review-pagination" class="pagination pagination-sm">
                        <?= $paginateLinks ?>
                    </ul>
                    <img src="/assets/img/loader/refresher.gif" class="-review-loader">
                    <div id="-review-showing" class="-showing">showing reviews 1 to 20 of 190</div>
                </nav>
                <div id="-review-hold" class="clearfix">
                    <?= $reviewList ?>
                </div>
                <nav class="clearfix">
                    <ul id="-review-pagination-footer" class="pagination pagination-sm">
                        <?= $paginateLinks ?>
                    </ul>
                    <img src="/assets/img/loader/refresher.gif" class="-review-loader">
                    <div id="-review-showing-footer" class="-showing">showing reviews 1 to 20 of 190</div>
                </nav>
            </div>
            <div class="col-lg-3">
                <div class="-review-overall clearfix">
                    <div class="-title">
                        Review Rating
                    </div>
                    <div class="-score">
                        <?= $data->business_info->total_rating->total_avg_rating ?>
                    </div>
                    <div class="-out-of">
                        Out of 5
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/app.js"></script>
</html>