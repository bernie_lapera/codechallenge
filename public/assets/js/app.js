$(document).ready(function(){

    /**
     * bind pagination
     */
    $('#-review-pagination, #-review-pagination-footer').on('click', 'a', function(event){
        event.preventDefault();
        if($(this).parent().hasClass('disabled')){
            return;
        }

        var page = $(this).data('page');

        if(!$.isNumeric(page)){
            return;
        }

        var loader = $(this).parents('.pagination:first').next();
        loader.show();

        $.ajax({
            type: 'GET',
            url: '/review/paginate/'+ page,
            success: function(response){
                try{
                    $('#-review-pagination, #-review-pagination-footer').html(response.paginateLinks);
                    $('#-review-hold').html(response.reviewList);
                    $('#-review-showing, #-review-showing-footer').html(response.showing);
                }
                catch(e){
                    alert('Invalid Response Format');
                }
                loader.fadeOut();
            },
            error: function(r){
                alert(r.responseText);
                loader.fadeOut();
            }
        });
    });
});