<?php
/**
 * html formatted shortcut dump function
 * 'helper functions' never located in index.php file...
 * @param $array
 */
function _printr($array){
    echo '<pre>'. print_r($array, 1) .'</pre>';
}

/**
 * register autoload
 */
spl_autoload_register(function ($class_name) {
    require '../'. str_replace("\\", "/", $class_name) .'.php';
});

/**
 * @todo update to more secure session management
 */
session_start();

/**
 * run route
 */
new app\controller\RouteController();